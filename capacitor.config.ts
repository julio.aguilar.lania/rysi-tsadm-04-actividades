import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'mx.lania.rysi.tsadm2022.actividades',
  appName: 'Control de Actividades',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
