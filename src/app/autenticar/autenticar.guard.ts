import { Injectable } from '@angular/core';
import { CanLoad, Route, UrlSegment, UrlTree } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { AutenticarService } from './autenticar.service';

@Injectable({
  providedIn: 'root'
})
export class AutenticarGuard implements CanLoad {

  constructor(private autSrv: AutenticarService,
    private navCtrl: NavController) {}

  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      console.log('canLoad');
    if (this.autSrv.estaLoggeado) {
      return true;
    }
    else {
      console.log('Redirigiendo a login');
      this.navCtrl.navigateForward('/ingreso');
    }
  }
}
