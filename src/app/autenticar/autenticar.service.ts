/* eslint-disable no-underscore-dangle */
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AutenticarService {

  private _estaLoggeado = false;
  constructor() { }

  get estaLoggeado() { return this._estaLoggeado;}

  login() {
    this._estaLoggeado = true;
  }

  logout() {
    this._estaLoggeado = false;
  }
}
