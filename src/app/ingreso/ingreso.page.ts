import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AutenticarService } from '../autenticar/autenticar.service';

@Component({
  selector: 'app-ingreso',
  templateUrl: './ingreso.page.html',
  styleUrls: ['./ingreso.page.scss'],
})
export class IngresoPage implements OnInit {

  constructor(private autSrv: AutenticarService,
    private navCtrl: NavController) { }

  ngOnInit() {
  }

  onEntrar() {
    this.autSrv.login();
    this.navCtrl.navigateForward('/actividades');
  }
}
