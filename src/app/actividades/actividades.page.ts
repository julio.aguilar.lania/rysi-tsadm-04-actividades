import { Component, OnInit } from '@angular/core';
import { Actividad } from './actividad.model';
import { AdminActividadesService } from './admin-actividades.service';

@Component({
  selector: 'app-actividades',
  templateUrl: './actividades.page.html',
  styleUrls: ['./actividades.page.scss'],
})
export class ActividadesPage implements OnInit {

  actividades: Actividad[];
  constructor(private actService: AdminActividadesService) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.actividades = this.actService.actividades;
  }

}
