import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Actividad } from '../actividad.model';
import { AdminActividadesService } from '../admin-actividades.service';

@Component({
  selector: 'app-detalle-actividad',
  templateUrl: './detalle-actividad.page.html',
  styleUrls: ['./detalle-actividad.page.scss'],
})
export class DetalleActividadPage implements OnInit {

  actividad: Actividad;
  constructor(
    private ruta: ActivatedRoute,
    private actSrv: AdminActividadesService,
    private navCtrl: NavController) { }

  ngOnInit() {
    this.ruta.paramMap
      .subscribe(params => {
        if (params.has('idAct')) {
          this.actividad = this.actSrv.getActividad(Number(params.get('idAct')));
        }
        else {
          this.navCtrl.navigateBack('/actividades');
        }
      });
  }

}
