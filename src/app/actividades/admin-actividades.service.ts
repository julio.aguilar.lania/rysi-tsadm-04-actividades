/* eslint-disable no-underscore-dangle */
import { Injectable } from '@angular/core';
import { Actividad } from './actividad.model';

@Injectable({
  providedIn: 'root'
})
export class AdminActividadesService {

  private _actividades: Actividad[] = [
    { id: 1, nombre: 'Estudiar Ruteo Angular',
    descripcion: 'Revisar material de ruteo de Angular y hacer ejercicios',
    estaTerminada: true,
    fechaInicio: new Date(2022,2,25), fechaTermino: new Date(2022,2,26),
    fechaEntrega: new Date(2022,2,31)},
    { id: 2, nombre: 'Registrar tema tesis',
    descripcion: 'Llenar registro de tesis en SICEL',
    estaTerminada: false,
    fechaInicio: new Date(2022,2,25), fechaTermino: null,
    fechaEntrega: new Date(2022,2,31)},
  ];
  constructor() { }

  get actividades() {
    return [...this._actividades];
  }

  agregarActividad(act: Actividad) {
    this._actividades.push({...act});
  }

  getActividad(id: number) {
    return {...this._actividades.find(a => a.id === id)};
  }
}
