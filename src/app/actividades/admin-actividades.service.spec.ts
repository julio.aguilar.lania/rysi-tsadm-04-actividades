import { TestBed } from '@angular/core/testing';

import { AdminActividadesService } from './admin-actividades.service';

describe('AdminActividadesService', () => {
  let service: AdminActividadesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AdminActividadesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
