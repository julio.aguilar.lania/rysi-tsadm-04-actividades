export interface Actividad {
  id: number;
  nombre: string;
  descripcion: string;
  estaTerminada: boolean;
  fechaInicio: Date;
  fechaEntrega: Date;
  fechaTermino: Date;
}
