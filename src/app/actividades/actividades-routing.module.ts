import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ActividadesPage } from './actividades.page';

const routes: Routes = [
  {
    path: '',
    component: ActividadesPage
  },
  {
    path: 'crear-actividad',
    loadChildren: () => import('./crear-actividad/crear-actividad.module').then( m => m.CrearActividadPageModule)
  },
  {
    path: 'editar-actividad/:idAct',
    loadChildren: () => import('./editar-actividad/editar-actividad.module').then( m => m.EditarActividadPageModule)
  },
  {
    path: 'detalle-actividad/:idAct',
    loadChildren: () => import('./detalle-actividad/detalle-actividad.module').then( m => m.DetalleActividadPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ActividadesPageRoutingModule {}
