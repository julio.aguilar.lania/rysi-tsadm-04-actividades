import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AutenticarService } from './autenticar/autenticar.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(private autSrv: AutenticarService,
    private navCtrl: NavController) {}

  onSalir() {
    this.autSrv.logout();
    this.navCtrl.navigateBack('/ingreso');
  }
}
